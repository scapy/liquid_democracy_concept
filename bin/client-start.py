#!/usr/bin/python2
"""
" Autor: Angel Tchorbadjiiski
" Email: angel.tchorbadjiiski@rwth-aachen.de
" Project: Liquid Democracy
" File Name: client-start
" Creation Date: 12.10.2011
" Last Modified: Mon 05 Mar 2012 05:18:10 PM CET
" Version: 0.0
" License: GPLv3
"""

# vim: set ai ts=4 sw=4 tw=80 ft=python:

import os
import sys
# set path for includes
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')
import urllib
from subprocess import check_output
from lib.common import *
from lib.client import *
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-s", dest="server", 
        help="Choose if you want to connect to 'register' or 'voting' server or check voting results/list. All of this options need further arguments from the options list.",
        metavar="[register|voting|check]")
parser.add_option("-n", dest="host", default='localhost',
        help="Hostname to connect to.", metavar="hostname")
parser.add_option("-p", dest="port", default=31337,
        help="Port to connect to.", metavar="portnum")
parser.add_option("-l", "--recvlen", dest="recvlen", default=1024,
        help="Receive buffer length.", metavar="recvlen")

# authentication options
parser.add_option("-a", "--auth", dest="auth", default='pwd',
        help="VR: Authentication type mechanism. Do not forget to supply additional arguments for the chosen type!",
            metavar="[pwd|key]")
# user, pass auth options
parser.add_option("-u", "--user", dest="user", default='angel',
        help="VR: Username for the authentication process.", metavar="username")
parser.add_option("-w", "--password", dest="pwd", default='test',
        help="VR: Password for the authentication process.", metavar="password")
# key auth options
parser.add_option("-k", "--keyid", dest="keyid", default=None,
        help="VR: Public key ID for the authentication process. Needed if key authentication used.", 
            metavar="keyid(int)")
parser.add_option("-f", "--keyfile", dest="keyfile", default=None,
        help="VR: File containing the public key for the authentication process. Needed if key authentication used.",
            metavar="keyfile")

parser.add_option("-v", "--votingfile", dest="votingfile", default=None,
        help="VC: File containing the voting credentials to be used.",
                  metavar="filename")
parser.add_option("-o", "--outputfile", dest="outputfile", default=None,
        help="VC: Name of the file to save voting receipt in.",
                  metavar="filename")

parser.add_option("-x", "--tokfile", dest="tokfile", default=None,
        help="CHECK:File with voting token results (-y & -z have to be specified also).", 
        metavar="filename")
parser.add_option("-y", "--listcreds", dest="vhashf", default=None,
        help="CHECK:File containing all used voting credentials (-x & -z have to be specified also).", 
        metavar="filename")
parser.add_option("-z", "--listvote", dest="votef", default=None,
        help="CHECK:File containing all (nonce,vote) pairs (-x & -y have to be specified also).", 
        metavar="filename")

parser.add_option("-d", "--download", dest="download", default=None,
        help="CHECK: Local path or URL to the voting list.",
        metavar="[/path/to/file|http://]")


(options, args) = parser.parse_args()

if options.server is None or options.server not in ('register','voting', 'check'):
	parser.print_help()
	sys.exit(0)


if options.server == 'register' and options.auth == 'key' and (options.keyid is None or int(options.keyid)%1 != 0) and options.keyfile is None:
    parser.print_help()
    sys.exit(1)

if options.server == 'check' and ((options.tokfile is None or options.vhashf is
        None or options.votef is None) and options.download is None):
    parser.print_help()
    sys.exit(2)

if options.server == 'check' and options.download is not None:
    # generate name for the signature file
    sigfile = options.download[0:options.download.rindex('.')]+'.sig'
    # load server key
    key = load_key('votingserver')
    pkey = key.publickey()

    if options.download[0:7] == 'http://':
        url = urllib.urlopen(urllib.quote(options.download))
        data = url.read()
        url.close()
        url = urllib.urlopen(urllib.quote(sigfile))
        sig = url.read()
        url.close()
    else:
        if not os.path.exists(options.download) or not os.path.exists(sigfile):
            print 'the supplied file or signature does not exist!'
            sys.exit(1)

        f = open(options.download, 'rb')
        data = f.read()
        f.close()
        f = open(sigfile, 'rb')
        sig = f.read()
        f.close()

    f = open('choices.txt', 'wb')
    f.write(data)
    f.close()

    hashsum = check_output(['/usr/bin/sha512sum', 'choices.txt'], True)

    if hashsum[0:hashsum.index(' ')] == sig:
        print 'successfully saved voting choices in choices.txt'
    else:
        os.system('rm choices.txt')
        print hashsum, sig
        print 'ERROR: signature verification failed!'


# init object
IO = VoterObject(options)
# start client
IO.run()
