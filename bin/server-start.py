#!/usr/bin/python2
"""
" Autor: Angel Tchorbadjiiski
" Email: angel.tchorbadjiiski@rwth-aachen.de
" Project: Liquid Democracy
" File Name: server-start.py
" Creation Date: 12.10.2011
" Last Modified: Mon 05 Mar 2012 05:18:03 PM CET
" Version: 0.0
" License: GPLv3
"""
# vim: set ai ts=4 sw=4 tw=80 ft=python:
import sys
import os
# append upper dir for the packages
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../')

from optparse import OptionParser
from lib.common import *
from lib.server import *

parser = OptionParser()
parser.add_option("-s", dest="server", 
                  help="Choose if you want to start 'register' or 'voting' server", metavar="[register|voting]")
parser.add_option("-n", dest="host", default='localhost',
                  help="Hostname to start on", metavar="hostname")
parser.add_option("-p", dest="port", default=31337,
                  help="Port to bind to", metavar="portnum")
parser.add_option("-l", "--recvlen", dest="recvlen", default=1024,
                  help="Receive buffer length", metavar="recvlen")

(options, args) = parser.parse_args()

if options.server is None or options.server not in ('register','voting'):
	parser.print_help()
	sys.exit(0)


# create server
c = ServerObject()
c.servtype = options.server
c.host = options.host
c.port = int(options.port)
c.size = int(options.recvlen)
c.run()
