#!/usr/bin/python2
"""
" Autor: Angel Tchorbadjiiski
" Email: angel.tchorbadjiiski@rwth-aachen.de
" Project: Liquid Democracy
" File Name: lists_create
" Creation Date: 12.10.2011
" Last Modified: Mon 05 Mar 2012 05:17:47 PM CET
" Version: 0.0
" License: GPLv3
"""

import os
import sys
# append upper dir for the packages
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')
import sqlite3
import datetime
from optparse import OptionParser

from lib.common import *

# directory to save the generated vr lists
dirname='lists/vr'

if not os.path.exists(dirname):
    os.makedirs(dirname, 0750)

parser = OptionParser()
parser.add_option('-f', '--file', dest='fname', default=None, 
        help='filename prefix for list generation', metavar='filename')
(opts, args) = parser.parse_args()

if opts.fname is None:
    fname = 'default'
else:
    fname = opts.fname

fname = fname+'_'+str(datetime.date.today())+'.txt'

print 'filename:', fname
print os.path.exists(fname)
if os.path.exists(dirname+'/'+fname):
    print 'file already exists! do you want to override it?'
    ans = raw_input('Yes/No? ').lower()
    if ans == 'no' or ans == 'n':
        print 'supply the filename as argument with the -f option'
        sys.exit(0)
    elif ans == 'yes' or ans == 'y':
        print 'filename {0} is being overwritten ...'.format(fname)
    else:
        print 'unknown answer given! exiting ...'
        sys.exit(1)


# connect to users db to test for existence of user
c1 = db_connect('sqlite', 'tokens')
c1.execute('select * from tokens')
res = c1.fetchall()

c2 = db_connect('sqlite', 'users')
f = open(dirname+'/'+fname, 'w')
f.write('uid;name;city;nonce;sig\n')
print 'writing file ...',
for i in res:
    (uid, nonce, sig) = i 
    c2.execute('select * from users where id=?', (uid,))
    (_,_,_,first, last, city)  = c2.fetchone()
    f.write('{0};{1} {2};{3};{4};{5}\n'.format(uid,first,last,city,nonce,sig))

print 'done'
f.close()
c2.close()
c1.close()
