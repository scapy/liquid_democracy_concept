#!/usr/bin/python2
"""
" Autor: Angel Tchorbadjiiski
" Email: angel.tchorbadjiiski@rwth-aachen.de
" Project: Liquid Democracy DA
" File Name: init_userdb
" Creation Date: 12.10.2011
" Last Modified: Mon 05 Mar 2012 05:17:54 PM CET
" Version: 0.1
" License: GPLv3
"""
# vim: set ai ts=4 sw=4 tw=80 ft=python:

import os
import sys
# append upper dir for the packages
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')

import sqlite3
from optparse import OptionParser
from lib.common import *


# id INTEGER PRIMARY KEY ASC, 
# username TEXT, 
# first TEXT, 
# last TEXT, 
# city TEXT, 
# passwd TEXT

parser = OptionParser()
parser.add_option('-a', '--add', dest='add', default=False, action="store_true",
        help='trigger user add', metavar='add')
parser.add_option('-d', '--delete', dest='delete', default=False, action="store_true",
        help='delete a user with the supplied id', metavar='delete')
parser.add_option('-m', '--modify', dest='modify', default=False, action="store_true",
        help='modify user with the supplied id', metavar='modify')
parser.add_option('-s', '--show', dest='show', default=False, action="store_true",
        help='list all users', metavar='show')

parser.add_option('-i', '--id', dest='id', default=None, 
        help='id to delete/modify', metavar='id')
parser.add_option('-u', '--username', dest='username', default=None, 
        help='username to add', metavar='username')
parser.add_option('-f', '--first', dest='first', default=None, 
        help='first name of user', metavar='first')
parser.add_option('-l', '--last', dest='last', default=None, 
        help='last name of user', metavar='last')
parser.add_option('-c', '--city', dest='city', default=None, 
        help='city of the person', metavar='city')
parser.add_option('-p', '--passwd', dest='passwd', default=None, 
        help='password of the user', metavar='passwd')

(opts, args) = parser.parse_args()


if not opts.add and not opts.delete and not opts.modify and not opts.show:
    print 'one of the options -a, -d, -m has to be specified'
    parser.print_help()
    sys.exit(1)

if (opts.add and opts.delete) or (opts.add and opts.modify) or (opts.modify and
        opts.delete):
    print 'only one of the three changing options allowed'
    sys.exit(2)

if (opts.delete or opts.modify) and opts.id is None:
    print 'by deleting modifying you have to specify the user id'
    parser.print_help()
    sys.exit(3)

if opts.show:
    c = db_connect('sqlite', 'users')
    c.execute("select * from users")
    res = c.fetchall()
    for i in res:
        print i

    sys.exit(0)

# connect to db
c = db_connect('sqlite', 'users')

if opts.add:
    if opts.username is None and opts.passwd is None:
        parser.print_help()
        sys.exit(4)


    c.execute('select count(*) from users where username=?', (opts.username,))

    (res,) = c.fetchone()

    if res == 0:
        c.execute('insert into users values (NULL,?,?,?,?,?)',
                (opts.username, hash_calc(opts.passwd), opts.first, opts.last, opts.city))
        print c.lastrowid
    else:
        print 'this username already exists, try another one!'
        
    c.close()


if opts.delete: 
    c.execute('delete from users where id=?', (int(opts.id),))


if opts.modify:
    for i in ('username', 'passwd', 'first', 'last', 'city'):
        if opts.i is not None:
            c.execute('update users set ?=? where id=?', (i, opts.i, int(opts.id),))


c.close()
