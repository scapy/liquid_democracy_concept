#!/usr/bin/python2
# vim: set ai ts=4 sw=4 tw=80 ft=python:
"""
" Autor: Angel Tchorbadjiiski
" Email: angel.tchorbadjiiski@rwth-aachen.de
" Project: LiquidDemocracy
" File Name: user_add_key
" Creation Date: 05.10.2011
" Last Modified: Mon 05 Mar 2012 05:17:41 PM CET
" Version: 0.1
" License: GPLv3
"""

import os
import sys
# append upper dir for the packages
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')

import sqlite3
from optparse import OptionParser

from lib.common import *


parser = OptionParser()
parser.add_option('-u', '--userid', dest='uid', default=None, 
        help='supply the user id to insert a key for', metavar='userid')
parser.add_option('-f', '--file', dest='fname', default=None,
        help='filename containing the key', metavar='filename')
parser.add_option('-l', '--list', dest='list', default=False,
        action="store_true", help='list all user ids', metavar='list')

(opts, args) = parser.parse_args()

if (opts.uid is None or int(opts.uid)%1 != 0 or opts.fname is None) and opts.list is False:
    parser.print_help()
    sys.exit(0)


if opts.list is True:
    c = db_connect('sqlite', 'users')
    c.execute('select id from users')
    for (_,) in c.fetchall():
        print _

    sys.exit(0)
   

# directory to save user keys
dirname='userkeys'


# connect to users db to test for existence of user
c = db_connect('sqlite', 'users')
c.execute('select count(*) from users where id=?', (opts.uid,))
(res,) = c.fetchone()

if not res:
    print 'no user with this uid found!'
    sys.exit(0)

c.close()

c = db_connect()
rsa_key = generate_key(opts.fname+str(res), 'nopub', dirname)

c.execute('insert into auth VALUES(?,?)', (res, str(rsa_key.publickey().__getstate__()),))

c.execute("select * from auth where id=?", (res,))
(res,) = c.fetchall()
c.close()

if res == 1:
    sys.exit(0)
else:
    sys.exit(1)

