#!/usr/bin/python2
"""
" Autor: Angel Tchorbadjiiski
" Email: angel.tchorbadjiiski@rwth-aachen.de
" Project:
" File Name: delegate
" Creation Date: 21.10.2011
" Last Modified: Mon 05 Mar 2012 05:18:23 PM CET
" Version: 0.0
" License: GPLv3
"""
# vim: set ai ts=4 sw=4 tw=80 ft=python:

import os
import sys
# append upper dir for the packages
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')

import sqlite3
from optparse import OptionParser

from lib.common import *
from lib.client import load_chain


parser = OptionParser()
parser.add_option('-c', '--check', dest='check', default=False,
        action="store_true", help='which token from the hash chain to select', metavar='check')
parser.add_option('-d', '--depth', dest='depth', default=None, 
        help='which token from the hash chain to select', metavar='depth')
parser.add_option('-i', '--infile', dest='infile', default=None,
        help='filename containing voting tokens', metavar='input filename')
parser.add_option('-o', '--outfile', dest='outfile', default=None,
        help='delegation token saved in this file', metavar='output filename')

(opts, args) = parser.parse_args()

if not opts.check and opts.depth is None and opts.infile is None and opts.outfile is None:
    parser.print_help()
    sys.exit(0)

# testing validity
if opts.check:
    # decide from which file to read the chain
    if opts.infile is None:
        res = load_chain()
    else:
        res = load_chain(opts.infile)

    # generate chain from shash to anchor
    depth = chain_depth(res['shash'], res['anchor'])
    if depth == False:
        print 'could not verify shash->anchor relation'
        sys.exit(2)

    # load pubkey and check signature
    key = load_key('registerserver')
    if not key.verify(res['anchor'], (long(res['sig']),)):
        print 'signature on anchor verifies to false!!!'
        sys.exit(3)

    print 'verification successful'
    sys.exit(0)

# just do it
if opts.depth is not None and opts.infile is not None and opts.outfile is not None:
    chain = load_chain(opts.infile)
    # calculate chain depth
    depth = chain_depth(chain['shash'], chain['anchor'])
    # check if valid
    if depth < int(opts.depth):
        print 'chain not deep enough to fulfill request!'
        sys.exit(4)

    # calc hash and save the chain
    dhash = chain_depth(chain['shash'], None, False, int(opts.depth))
    if save_chain(chain['anchor'], chain['sig'], dhash, opts.outfile) is True:
        print 'chain saved in file:', opts.outfile
    else:
        print 'saving failed!'




sys.exit(1)
# directory to save user keys
dirname='userkeys'


# connect to users db to test for existence of user
c = db_connect('sqlite', 'users')
c.execute('select count(*) from users where id=?', (opts.uid,))
(res,) = c.fetchone()

if not res:
    print 'no user with this uid found!'
    sys.exit(0)

c.close()

c = db_connect()
rsa_key = generate_key(opts.fname+str(res), 'nopub', dirname)

c.execute('insert into auth VALUES(?,?)', (res,
    str(rsa_key.publickey().__getstate__()),))

c.execute("select * from auth where id=?", (res,))
data = c.fetchall()
for i in data:
    print i

c.close()
