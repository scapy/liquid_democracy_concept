#!/usr/bin/python2
"""
" Autor: Angel Tchorbadjiiski
" Email: angel.tchorbadjiiski@rwth-aachen.de
" Project: Liquid Democracy
" File Name: choices_generate
" Creation Date: 12.10.2011
" Last Modified: Mon 05 Mar 2012 05:15:03 PM CET
" Version: 0.0
" License: GPLv3
"""
# vim: set ai ts=4 sw=4 tw=80 ft=python:

import os
import sys
# append upper dir for the packages
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')

import sqlite3
import datetime

from optparse import OptionParser
from subprocess import check_output

from lib.common import *

dirname='lists/vc'

if not os.path.exists(dirname):
    os.makedirs(dirname, 0750)

parser = OptionParser()
parser.add_option('-f', '--file', dest='fname', default='choices.txt', 
        help='Filename (ext *.txt) to save the possible choices in.', metavar='filename')
(opts, args) = parser.parse_args()

#os.system('ls -lt')

arr = {} 
for i in xrange(1,50):
    arr[i] = 'vote'+str(i)

data = parse_output_data(arr)

# write data to *.txt
f = open(dirname+'/'+opts.fname, 'wb')
f.write(data)
f.close()

# use sha512sum for generating a hash
sig = check_output(['/usr/bin/sha512sum', dirname+'/'+opts.fname], True)
# save signature in *.sig
f = open(dirname+'/'+opts.fname[0:opts.fname.rindex('.')]+'.sig', 'wb')
f.write(sig[0:sig.index(' ')])
f.close()

sys.exit(0)
