#!/usr/bin/python2
"""
" Autor: Angel Tchorbadjiiski
" Email: angel.tchorbadjiiski@rwth-aachen.de
" Project: Liquid Democracy
" File Name: results_generate
" Creation Date: 12.10.2011
" Last Modified: Mon 05 Mar 2012 05:17:26 PM CET
" Version: 0.0
" License: GPLv3
"""
# vim: set ai ts=4 sw=4 tw=80 ft=python:

import os
import sys
# append upper dir for the packages
sys.path.append(os.path.dirname(os.path.abspath(__file__))+'/../../')

import sqlite3
import datetime
from optparse import OptionParser
from lib.common import *

dirname='lists/vc'

if not os.path.exists(dirname):
    os.makedirs(dirname, 0750)

#parser = OptionParser()
#parser.add_option('-a', '--add', dest='add', default=False, action="store_true",
#        help='trigger user add', metavar='add')
#parser.add_option('-d', '--delete', dest='delete', default=False, action="store_true",
#        help='delete a user with the supplied id', metavar='delete')
#parser.add_option('-m', '--modify', dest='modify', default=False, action="store_true",
#        help='modify user with the supplied id', metavar='modify')
#parser.add_option('-s', '--show', dest='show', default=False, action="store_true",
#        help='list all users', metavar='show')
#
#parser.add_option('-i', '--id', dest='id', default=None, 
#        help='id to delete/modify', metavar='id')
#parser.add_option('-u', '--username', dest='username', default=None, 
#        help='username to add', metavar='username')
#parser.add_option('-f', '--first', dest='first', default=None, 
#        help='first name of user', metavar='first')
#parser.add_option('-l', '--last', dest='last', default=None, 
#        help='last name of user', metavar='last')
#parser.add_option('-c', '--city', dest='city', default=None, 
#        help='city of the person', metavar='city')
#parser.add_option('-p', '--passwd', dest='passwd', default=None, 
#        help='password of the user', metavar='passwd')
#
#(opts, args) = parser.parse_args()




# connect to db
c = db_connect('sqlite', 'voting')

# generate (anchor, sig, vhash) tuples and save them to file
c.execute('select anchor, sig, vhash, counter from voting order by anchor asc')

f1 = open(dirname+'/'+'verify_vhash_'+str(datetime.date.today())+'.txt', 'wb')
f1.write(';'.join(('anchor','signature','vhash','counter'))+'\n')
for i in c.fetchall():
    f1.write(i[0]+';'+i[1]+';'+i[2]+';'+str(i[3])+'\n')

f1.close()
print 'vhash verification file successfully written!'

# generate (nonce,vote) tuples in file
c.execute('select nonce, vote from voting order by nonce asc')
f2 = open(dirname+'/'+'verify_vote_'+str(datetime.date.today())+'.txt', 'wb')
f2.write(';'.join(('nonce','vote'))+'\n')
for j in c.fetchall():
    f2.write(';'.join(j)+'\n')

f2.close()
print 'vote verification file successfully written!'

c.execute('create view results as select vote,max(counter) from voting group by anchor')
c.execute('select vote,count(vote) from results group by vote')
for k in c.fetchall():
    print k

c.execute('drop view results')

sys.exit(1)
