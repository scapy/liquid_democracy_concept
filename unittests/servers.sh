#!/bin/bash

. ./unittests/declarations.sh

function servers_start() {
   # start vr server
   if [[ -x "${LOCDIR}/bin/server-start" ]]; then
      screen -mdL -S "VRServ"  ${LOCDIR}/bin/server-start -s register -p ${VRPORT}
      echo "started1: $?";
      sleep 1;

   fi

   # start vc server
   if [[ -x "${LOCDIR}/bin/server-start" ]]; then
      screen -mdL -S "VCServ"  ${LOCDIR}/bin/server-start -s voting -p ${VCPORT}
      echo "started2: $?";
      sleep 1;
   fi
}


function servers_stop() {
   # check if servers running
   if [[ $(screen -ls|grep Serv|wc -l) -gt 0 ]]; then
      vrserv=$(screen -ls|grep "VRServ"|awk -F. '{print $1}'|awk '{print $1}');
      kill -9 ${vrserv/ /};
      vcserv=$(screen -ls|grep "VCServ"|awk -F. '{print $1}'|awk '{print $1}');
      kill -9 ${vcserv/ /};
   else
      echo "No services running, nothing can be killed!";
   fi
}


if [[ $# -eq 0 ]]; then
   echo "usage: $0 [start|stop|check]";
   exit 0;
fi


if [[ 1 -ne $(which screen| wc -l) ]]; then
   echo "no screen command found!";
   echo "install screen to test the system";
   exit 1;
fi

# clear possibly dead processes
screen -wipe > /dev/null
sleep 1;

if [[ "$@" = "start"  ]]; then
   servers_start;
elif [[ "$@" = "stop" ]]; then 
   servers_stop;
elif [[ "$@" = "check" ]]; then
   echo "$(screen -ls|grep Serv)";
else
   echo "no such command!"
   exit 1;
fi

exit 0;

