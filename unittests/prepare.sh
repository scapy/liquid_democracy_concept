#!/bin/bash

. ./unittests/declarations.sh

if [[ $# -lt 1 ]]; then
   echo "usage $0 [clear|normal]";
   exit 0;
fi


function add_user() {
   # default values
   if [[ $# -eq 1 ]]; then 
      UNAME="angel";
      FNAME="Angel";
      LNAME="Tchorbadjiiski";
      CITY="Aachen";
      PASS="default";
   else
      echo "username:"
      read UNAME
      echo "first name:"
      read FNAME
      echo "last name:"
      read LNAME
      echo "city:"
      read CITY
      echo "pass:"
      read PASS
   fi

   if [[ -x "${LOCDIR}/bin/vr/user_modify" ]]; then
      LOCUID="$(${LOCDIR}/bin/vr/user_modify -a -u "${UNAME}" -f "${FNAME}" -l "${LNAME}" -c "${CITY}" -p "${PASS}")"
   else
      exit 2;
   fi
   if [[ $LOCUID =~ "this username already exists," ]]; then
      echo "Error::: ${LOCUID}";
      exit 2;
   fi

   add_user_key ${LOCUID} "${UNAME}";
}


function add_user_key() {
   if [[ $# -lt 2 ]]; then
      echo "not enough arguments";
      exit 1;
   fi

   if [[ -x "${LOCDIR}/bin/vr/user_add_key" ]]; then
      echo -n "adding key for uid ${1} with username ${2} ... ";
      ${LOCDIR}/bin/vr/user_add_key -u ${1} -f ${2}
      echo -e "${CLRGRN}done${CLREND}";
   else
      exit 3;
   fi
}

# check if to clear all
if [[ "$@" = "clear" ]]; then
   # stop possibly running servers
   if [[ -x "${LOCDIR}/unittests/servers.sh" ]]; then
      if [[ $(${LOCDIR}/unittests/servers.sh check | wc -l) -gt 1 ]]; then
         ${LOCDIR}/unittests/servers.sh stop
      fi
   fi
   # do the job
   clear_all;

   add_user "default";
else 
   echo "number of users to add:";
   read CNT;
   TCNT={$CNT};
   while [ ${CNT} -gt 0 ]; do
      add_user;
      let "CNT -= 1";
   done
fi

exit 0;
