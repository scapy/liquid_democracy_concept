#!/bin/bash

. ./unittests/declarations.sh;

if [[ $# -lt 1 ]]; then
   echo "usage $0 [check|delegate]";
   exit 0;
fi


# check authorisation data from VR
function check_vblock() {
   if [[ -f "${LOCDIR}/vblock.txt" ]]; then
      VBLOCK="vblock.txt";
      echo "default file vblock.txt found - using it...";
   else
      echo "file with data to check:";
      read VBLOCK;
   fi

   if [[ ! -r ${LOCDIR}/${VBLOCK} ]]; then
      echo "file does not exist or is not readable!!!";
      exit 1;
   fi

   if [[ -x "${LOCDIR}/bin/client/delegate" ]]; then
      echo -e "${CLRGRN}"
      ${LOCDIR}/bin/client/delegate -c -i ${VBLOCK}
      echo -e "${CLREND}";
   else
      echo "the binary delegate could not be found!";
      exit 2;
   fi
}


# delegation routine
function delegate() {
   if [[ $# -gt 0 ]]; then
      DEPTH="256";
      IFILE="vblock.txt";
      OFILE="delegated.txt";
   else
      echo "give in the delegation depth:";
      read DEPTH;
      
      echo "file to delegate from:";
      read IFILE;

      if [[ ! -f "${LOCDIR}/${IFILE}" ]]; then
         echo "this file does not exist! try again:";
         read IFILE;
      fi

      echo "file to delegate to:";
      read OFILE;
      if [[ -f "${LOCDIR}/${OFILE}" ]]; then
         echo "this file does exist!!! give in the name again to overwrite:";
         read OFILE;
      fi
   fi

   echo "starting delegation with";
   echo "depth == ${DEPTH}";
   echo "from file ${IFILE}";
   echo "to file ${OFILE}";
   if [[ -x "${LOCDIR}/bin/client/delegate" ]]; then
      echo -e "${CLRGRN}"
      ./bin/client/delegate -d ${DEPTH} -i ${IFILE} -o ${OFILE};
      echo -e "${CLREND}";
   else
      echo "the delegate binary does not exist!";
      exit 3;
   fi
   echo;
   
}


LOCDIR=$(pwd);
if [[ ! "${LOCDIR}" =~ .+/code/LD ]]; then
   echo "You have to call this script from the $GITREP/code/LD/ directory" ;
   exit 0;
fi

# use default values
if [[ "$@" =~ "default" ]]; then
   D="default";
fi

# check if to clear all
if [[ "$@" =~ "check" ]]; then
   check_vblock ${D};
else
   delegate ${D};
fi

exit 0;
