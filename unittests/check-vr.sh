#!/bin/bash

. ./unittests/declarations.sh

function get_creds() {
   # default values ?
   if [[ $# -gt 0 ]]; then
      KID="1";
      FPATH="angel1.priv";
   else 
      echo "id to identify:"
      read KID
      echo "name of keyfile:"
      read FPATH
   fi

   echo -n "get credentials on the client ... "
   if [[ -x "${LOCDIR}/bin/client-start" ]]; then
      echo -e "${CLRGRN}"
      ${LOCDIR}/bin/client-start -s register -a key -p ${VRPORT} -k ${KID} -f "userkeys/${FPATH}"
      echo -e "${CLREND}";
      if [[ $? -ne 0 ]]; then
         echo " $? failed"
         exit 1;
      fi
   else
      exit 2;
   fi
}

# start servers over external script
if [[ -x "${LOCDIR}/unittests/servers.sh" ]]; then
   if [[ $(${LOCDIR}/unittests/servers.sh check| wc -l) -lt 2 ]]; then
      ${LOCDIR}/unittests/servers.sh start
   fi
fi

# get credentials for a given user from the VR server
if [[ $# -gt 0 ]]; then
   get_creds "defaults";
else
   get_creds;
fi

exit 0;
