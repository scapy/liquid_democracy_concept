#!/bin/bash

. ./unittests/declarations.sh

# dir with unit test scripts
UT="unittests";

echo "clear old data and prepare for new run: ";
${LOCDIR}/${UT}/prepare.sh clear;
echo ;

echo "authenticate to VR and acquire voting creds: ";
${LOCDIR}/${UT}/check-vr.sh default; 
echo ;

echo "check validity of voting creds: ";
${LOCDIR}/${UT}/delegate.sh check;
echo ; 

echo "delegate voting creds: ";
${LOCDIR}/${UT}/delegate.sh delegate default;
echo ; 

echo "check validity of DELEGATED voting creds: ";
${LOCDIR}/${UT}/delegate.sh check;
echo ; 

echo "vote: ";
${LOCDIR}/${UT}/check-vc.sh vote default;
echo ; 

echo "check voting results: ";
${LOCDIR}/${UT}/check-vc.sh check default;
echo ; 

echo -n "stopping VR and VC servers: ";
${LOCDIR}/${UT}/servers.sh stop;
echo -e "${CLRGRN}done${CLREND}";

exit 0;
