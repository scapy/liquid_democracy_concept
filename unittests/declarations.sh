#!/bin/bash

################################################################################
# DECLARATIONS
################################################################################
# color vars
CLRGRN="\e[0;32m";
CLREND="\e[0m";
# local port for servers
VRPORT=31331
VCPORT=31332
################################################################################

# clear all data created from the user
function clear_all() {
   # check if all files there
   if [[ ! -x dbs ]]; then
      mkdir "${LOCDIR}/dbs";
      if $? -ne 0; then
         exit 1;
      fi
   fi

   # clear old databases
   if [[ $(ls ${LOCDIR}/dbs/ | wc -l) -gt 0 ]]; then
      echo -n "clearing databases ... ";
      rm ${LOCDIR}/dbs/*.sqlite3;
      echo -e "${CLRGRN}done${CLREND}";
   fi

   # clear user keys
   if [[ -x "${LOCDIR}/userkeys" && $(ls ${LOCDIR}/userkeys/ | wc -l) -gt 0 ]]; then
      echo -n "removing user keys ... ";
      rm ${LOCDIR}/userkeys/*;
      echo -e "${CLRGRN}done${CLREND}";
   fi

   # clear old lists
   if [[ -x "${LOCDIR}/lists" ]]; then
      rm ${LOCDIR}/lists/vr/* 2>/dev/null;
      rm ${LOCDIR}/lists/vc/* 2>/dev/null;
   fi

   # clear temporary files 
   if [[ -n "$(ls *.txt 2>/dev/null)" ]]; then
      echo -n "removing *.txt files ... ";
      rm *.txt;
      rm screenlog*
      echo -e "${CLRGRN}done${CLREND}";
   fi
}

LOCDIR=$(pwd);
if [[ ! "${LOCDIR}" =~ .+/code/LD ]]; then
   echo "You have to call this script from the GITREP/code/LD/ directory" ;
   exit 0;
fi
