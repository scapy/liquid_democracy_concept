#!/bin/bash

. ./unittests/declarations.sh


if [[ $# -ne 1 && $# -ne 2 ]]; then
   echo "usage $0 [vote|check] [default]";
   exit 0;
elif [[ $1 != "check" && $1 != "vote" ]]; then
   echo "usage $0 [vote|check] [default]";
   exit 0;
else
   echo -n;
fi

# start servers over external script
if [[ -x "${LOCDIR}/unittests/servers.sh" ]]; then
   if [[ $(${LOCDIR}/unittests/servers.sh check| wc -l) -lt 2 ]]; then
      ${LOCDIR}/unittests/servers.sh start
   fi
fi

if [[ $@ =~ "default" ]]; then
   D="default";
fi

if [[ -x "${LOCDIR}/bin/client-start" ]]; then
   # vote 
   if [[ ${1} = "vote" ]]; then
      if [[ -f "${LOCDIR}/vblock.txt" ]]; then
         VBLOCK="vblock.txt";

         if [[ -z ${D} ]]; then
            echo "file vblock.txt exists!!!";
            echo "if you want to use it for voting please press ENTER";
            echo "else give in the filename to use:";
            
            read INPUT;
            if [[ -n ${INPUT} ]]; then
               VBLOCK=${INPUT};
            fi
         fi 
      else
         echo "vblock.txt does not exist!"
         echo "give in the filename with voting credentials:";
         read VBLOCK;
      fi 

      if [[ ! -f "${LOCDIR}/${VBLOCK}" ]]; then
         echo "file ${VBLOCK} does not exist!";
         exit 2;
      fi
         
      ${LOCDIR}/bin/client-start -s voting -o receipt.txt -d ${LOCDIR}/lists/vc/choices.txt -p ${VCPORT} -v "${VBLOCK}";
   # check voting results 
   elif [[ ${1} = "check" ]]; then
      # generate the result lists on the VC
      if [[ ! -x "${LOCDIR}/bin/vc/results_generate" ]]; then
         echo "ERROR::: binary not found!";
         exit 5;
      fi
      echo -n "generating result lists: ";
      ${LOCDIR}/bin/vc/results_generate;
      echo -e "${CLRGRN}done${CLREND}";

      if [[ ${D} ]]; then
         LISTS=$(ls -t1 ${LOCDIR}/lists/vc/* | head -n 2)
         LARR=(${LISTS//;/})
         # ${LARR[0]} == hashes list
         # ${LARR[1]} == votes list
         
         RESULT="receipt.txt";
         CLIST=${LARR[0]};
         VLIST=${LARR[1]};
         echo 
      else 
         # supply needed parameters
         echo "supply the filenames (relatively to CWD) of the following files:";
         echo "1. voting output";
         read RESULT;

         echo "(the next two files possibly lie under ./lists/vc/)";
         echo "2. voting server credentials (vhash list)";
         read CLIST;
         
         echo "3. voting server voting list";
         read VLIST;
      fi 
      
      # check file existence
      if [[  ! -r ${LOCDIR}/${RESULT} ]]; then
         echo "file |${RESULT}| (result file) does not exist!";
         exit 3;
      fi
      if [[ ! -r ${CLIST} ]]; then 
         echo "file ${CLIST} (clist) does not exist!";
         exit 4;
      fi
      if [[ ! -r ${VLIST} ]]; then
         echo "file ${VLIST} (vlist) does not exist!";
         exit 5;
      fi

      echo "DBG::: ";
      echo "${LOCDIR}/bin/client-start -p ${VCPORT} -s check -x ${LOCDIR}/${RESULT} -y ${CLIST} -z ${VLIST};";
      # check voting results using server lists
      ${LOCDIR}/bin/client-start -p ${VCPORT} -s check -x "${LOCDIR}/${RESULT}" -y ${CLIST} -z ${VLIST};
   else
      echo "unknown option supplied!";
      exit 4;
   fi
else 
   echo "${LOCDIR}/bin/client-start not found!";
   exit 1;
fi 

exit 0;
