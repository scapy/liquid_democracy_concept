#!/usr/bin/python2

"""
A library containing all the code needed by the Voting Register (VR) and Voting
Computer (VC) instances. 
"""

# basic functions needed
from lib.common import *

# debugging/timing
import time

import os
import sys
import socket
import select
import threading
import sqlite3
from base64 import b64encode,b64decode

from gdata.Crypto.PublicKey.RSA import *
from gdata.Crypto.Util.number import bytes_to_long,long_to_bytes

import logging

logger = logging.getLogger('myserver')
hdlr = logging.FileHandler('/tmp/myserver.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.WARNING)


class ServerObject:
    def __init__(self):
        self.host = ''
        self.port = 31337
        self.backlog = 5
        self.servtype = ''
        self.recvlen = 1024
        self.servsock = None
        self.threads = []

    def open_socket(self):
        try:
	
            self.servsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.servsock.bind((self.host,self.port))
            self.servsock.listen(self.backlog)
        except socket.error, (value,message):
            if self.servsock:
                self.servsock.close()
            print "Could not open socket: " + message
            sys.exit(1)

    def client_status():
        print "client_status()"
        print length(self.threads)
        for x in self.threads:
            print x

    def run(self):
        self.open_socket()
        input = [self.servsock,sys.stdin]
        running = 1
        while running:
            inputready,outputready,exceptready = select.select(input,[],[])

            for s in inputready:

                if s == self.servsock:
                    # handle the server socket
                    c = HandleClient(self.servsock.accept(), self.servtype)
                    c.start()
                    self.threads.append(c)

                elif s == sys.stdin:
                    # handle standard input
                    junk = sys.stdin.readline()
                    running = 0

        # close all threads

        self.servsock.close()
        for c in self.threads:
            c.join() 



class HandleClient(threading.Thread):
    """threaded client handler"""

    def __init__(self,(client,ipaddr), servtype):
        threading.Thread.__init__(self)
        self.client = client
        # hack for pyreverse
        self.ipaddr = ''
        self.ipaddr = ipaddr
        self.recvlen = 1024
        # hack for pyreverse
        self.servtype = ''
        self.servtype = servtype
        self.running = 0

    def run(self):
        self.running = 1
        # start encryption
        self.client = enc_start(self.client, True)
        while self.running:
            # doing the thread stuff
            if self.servtype == 'register':
                self.register_server()
            elif self.servtype == 'voting':
                self.voting_server()
            else:
                print 'error: false server name given'

            #end here
            break


    def register_server(self):
        print 'register server:'
        # timing 
        ts = time.clock()

        if not self.check_auth():
            self.client.close()
            logger.error('auth failed!')
            return

        data = self.data_get()
        if data['error'] == 'True':
            logger.error('no bmsg sent, error occurred!')
            self.data_send({'error':True})
            self.client.close()
            return

        key = load_key('registerserver')
        sbhash = long(sign_msg(key, long(data['bmsg'])))
        pubkey = key.publickey()
        # check if signature ok before transfering
        if pubkey.verify(long(data['bmsg']), (sbhash,)) is True:
            self.data_send({'sbhash':sbhash,'error':None})
        else:
            logger.error('signature verification failed!')
            self.data_send({'error':True})

        self.client.close()
        
        te=time.clock()
        print 'time needed: ', te-ts



    def voting_server(self):
        print 'voting server: '
        ts = time.clock()
        
        # load keys needed for the voting
        votingkey = load_key('votingserver')
        registerkey = load_key('registerserver', ktype='pub')
        # exit on error
        if not (votingkey and registerkey):
            logger.error('loading keys (voting.priv or register.pub) failed')
            self.data_send({'error':True})
            self.client.close()
            return

        # check auth (anchor + sig)
        authdata = self.data_get()
        if not check_sig(registerkey, authdata['anchor'], authdata['sig']):
            logger.error('anchor signature verification failed!')
            self.data_send({'error':True})
            self.client.close()
            return

        self.data_send({'auth':True,'error':False})

        # start diffie-hellman nonce negotiation
        dhstart = self.data_get()
        if not dhstart['dh'] == 'start':
            logger.error('DH key agreement not started!')
            self.data_send({'error':True})
            self.client.close()
            return

        # generate dh value and send it to the client
        dh_val = dh_gen(False)
        # send generator g, prime p and public value to client
        self.data_send({'a.g':b64encode(dh_val.g).replace('=','@'),
            'a.p':b64encode(dh_val.p).replace('=','@'),
            'a.pub':b64encode(dh_val.pub).replace('=','@'),
            'error':False})

        # receive client part
        dh_exchange = self.data_get()
        if dh_exchange['error'] == 'True':
            logger.error('DH key agreement problem by receiving b.pub!')
            self.data_send({'error':True})
            self.client.close()
            return

        # calculate nonce
        nonce = dh_calc(dh_val,	b64decode(dh_exchange['b.pub'].replace('@','=')))

        # sanity check for values {0,1} for client pub 
        if nonce > dh_val:
            self.data_send({'error':False})
        else:
            logger.error('client used unsafe value ({0,1})')
            self.data_send({'error':True})
            self.client.close()
            return

        # receive blinded voting creds
        tmp = self.data_get()
        bmsg = long(tmp['bmsg'])

        # sign data and send back
        signed_bdata = sign_msg(votingkey, bmsg)
        self.data_send({'sbmsg':signed_bdata,'error':False})

        # receive blinding vhash
        vdata = self.data_get()
        vhash = vdata['vhash']
        
        # check if vhash already used for voting
        c = db_connect('sqlite', 'voting')
        c.execute('select count(*) from voting where vhash=? and anchor=?',
                (vhash, authdata['anchor'],))
        (res,) = c.fetchone()
        c.close()
        if res != 0:
            logger.error('(vhash,anchor) combination ({0},{1}) was already used for voting!'.format(vhash,authdata['anchor']))
            self.data_send({'error':True, 'errmsg':'the pair (vhash,anchor) you supplied was already used for voting!'})
            self.client.close()
            return
      

        vblock = long_to_bytes(vdata['vblock'])

        # check vhash validity
        hdepth = in_hash_chain(vhash, authdata['anchor'])
        if hdepth == False:
            logger.error('vhash to anchor verification failed!')
            self.data_send({'error':True, 'errmsg':'vhash to anchor calculation failed!'})
            self.client.close()
            return

        # unblind data and check sig
        uvoting = votingkey.unblind(long(signed_bdata), vhash)
        if not check_sig(votingkey, vblock, uvoting):
            logger.error('vblock check failed!')
            self.data_send({'error':True})
            self.client.close()
            return 

        # check nonce
        parted = parse_input_data(vblock)
        if int(parted['nonce']) != nonce:
            logger.error('erroneous nonce value!')
            self.data_send({'error':True})
            self.client.close()
            return 

        # save vote and generate receipt
        receipt = self.vote_save(vhash, authdata['anchor'],  authdata['sig'],
                parted['nonce'], parted['vote'], hdepth)

        # receipt generated
        if not receipt:
            logger.error('vote could not be saved')
            self.data_send({'error':True, 'errmsg':'vote could not be saved'})
            self.client.close()
            return

        # sign receipt and send it over to the client
        rsig = sign_msg(votingkey, receipt)
        self.data_send({'receipt':receipt, 'rsig':rsig, 'error':None})

        self.client.close()

        te=time.clock()
        print 'vote timing: ', te-ts


    def vote_save(self, vhash, anchor, sig, nonce, vote, depth):
        """Save the given voting credentials and return a receipt for the vote"""

        c = db_connect('sqlite', 'voting')
        c.execute('insert into voting VALUES (NULL,?,?,?,?,?,NULL,?)',
                (anchor,sig,vhash,nonce,vote,depth,))

        lid = c.lastrowid
        if lid < 1:
            return False

        receipt = hash_calc(str(lid)+vhash+anchor+str(sig)+nonce+vote)
        c.execute('update voting set receipt=? where id=?',(receipt,lid,))

        return receipt


    def data_get(self):
        """receive data from client socket and parse it"""
        return parse_input_data(self.client.recv(self.recvlen).rstrip())


    def data_send(self, data):
        """prepare data for transmission and send it out to the client"""
        self.client.send(parse_output_data(data))


    def check_auth(self):
        """Get user authentication information"""
        auth = self.data_get()

        if auth['error'] == 'True':
            return False

        if auth['type'] == 'pubkey':
            return self.auth_pkey(auth['uid'])
        else:
            print "auth type not supported!"
            return False


    def auth_pkey(self, uid):
        """Check auth over pkey crypto"""
        # load key with the given id
        pubkey = load_key_db(uid)
        if pubkey is None:
            logger.error('public key for supplied id could not be loaded!')
            self.data_send({'errmsg':'no key with this id found','error':True})
            return False

        # generate random secret for auth
        nonce = b64encode(os.urandom(128)).replace('=','@')
        (data,) = pubkey.encrypt(nonce, len(nonce))
        benc_data = b64encode(data).replace('=','@') 
        self.data_send({'enc':benc_data,'error':None})
        idata = self.data_get()

        if idata['error'] != 'True' and idata['dec'] == nonce and check_sig(pubkey, nonce, idata['sig']):
            c = db_connect('sqlite', 'tokens')
            # check if creds already given out
            c.execute('select count(*) from tokens where uid=?', (uid))
            (tres,) = c.fetchone()
            if tres != 0:
                print 'tres: ', tres 
                self.data_send({'error':True, 'errmsg':'user already has voting creds!'})
                return False

            c.execute('insert into tokens values (?,?,?)', (uid, nonce, idata['sig'],))
            self.data_send({'ok':True, 'error':None})
            return True
        else:
            self.data_send({'error':True})
            return False



            

