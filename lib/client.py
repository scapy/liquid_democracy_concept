#!/usr/bin/python2
"""
"""
# timing/debugging
import time

import os
import sys
import socket
import re
from random import getrandbits
from base64 import b64encode, b64decode

#crypto modules
from gdata.Crypto.PublicKey import RSA
from gdata.Crypto.Util.number import bytes_to_long,long_to_bytes

# import local package functions
from lib.common import *



def sock_connect(host, port):
	"""Connect to server with (host,port) information"""
	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect((host, port))
	except socket.error, msg:
		sock.close()
		print 'cound not open socket or connect to remote end'
		sys.exit(255)

	return sock



def load_chain(vblock='./vblock.txt'):
    """Load voting token from a local file"""
    if os.path.exists(vblock) and os.path.getsize(vblock):
        fd = open(vblock, 'rb')
        data = fd.read()
        fd.close()
        return parse_input_data(data)
    else:
        return None




def save_voting_results(anchor, asig, vhash, nonce, vote, receipt, rsig, filename=None):
    """Saves voting result in a file"""
    if filename is None:
        fname = raw_input('Give in a filename to save the data in: ')
    else:
        fname = filename

    return fwrite(fname, 'wb', 
        parse_output_data(
            {
                'anchor':anchor, 
                'asig':asig, 
                'vhash':vhash, 
                'nonce':nonce, 
                'vote':vote, 
                'receipt':receipt, 
                'rsig':rsig
            }
        )
    )



def check_voting_results(tokfile, vhashf, votef):
    if not os.path.exists(tokfile):
        return False
    
    f = open(tokfile, 'rb')
    # read tokens
    tok = parse_input_data(f.readline())

    # check if data in both files
    if not (check_data_in_file(votef, ';'.join((tok['nonce'],tok['vote']))) and check_data_in_file(vhashf,
            ';'.join((tok['anchor'],tok['asig'],tok['vhash'])))):
        print "data not in file"
        return False

    locarr = {}
    # get all lines with this anchor
    for line in open(vhashf, 'rb'):
        if re.search("^"+tok['anchor'], line):
            tmp = line.split(';')
            locarr[tmp[2]] = tmp[3]


    # check if own vhash is overvoted
    if tok['vhash'] in locarr.keys() and max(locarr.iterkeys(), key=lambda k:locarr[k]) == tok['vhash']: 
        return True
    else:
        print 'You have been outvoted!'
        return False



def check_data_in_file(fname, data):
    """Checks if given data set is in file"""
    if not os.path.exists(fname):
        return False

    for line in open(fname, 'rb'):
        if data in line:
            return True
        

    return False



class VoterObject:

    def __init__(self, opts):
        """Initalize properties"""
        self.host = str(opts.host)
        self.port = int(opts.port)
        self.recvlen = int(opts.recvlen)
        # hack for pyreverse
        self.keyfile = ''
        self.keyfile = opts.server + 'server'
        # hack for pyreverse
        self.opts = []
        self.opts = opts


    def run(self):
        self.connect()
        if self.keyfile[0:8] == 'register':
            self.interact_vr()
        elif self.keyfile[0:6] == 'voting':
            self.interact_vc()
        else:
            self.check_voting_results()


    def proxy_connect(self):
        """Connect to anonymous proxy (dummy)"""
        pass

       
    def connect(self):
        """Start encrypted connection"""
#        proxy connect?
        if self.keyfile[0:6] == 'voting':
            self.proxy_connect()

        sock = sock_connect(self.host, self.port)
        # start encryption
        self.sock = enc_start(sock)


    def data_get(self):
        """receive data from socket connection"""
        return parse_input_data(self.sock.recv(self.recvlen).rstrip())


    def data_send(self, data):
        """prepare data in dict format and send it out"""
        self.sock.send(parse_output_data(data))


    def auth_pkey(self, keyfile, uid):
        """pubkey auth routine"""
        self.data_send({'type':'pubkey', 'uid':uid, 'error':None})
        enc_pack = self.data_get()
        if enc_pack['error'] == 'True':
            print 'Error: ', enc_pack['errmsg']
            return False

        enc_data = b64decode(enc_pack['enc'].replace('@','='))

        rsakey = load_key(keyfile, 'priv', 'userkeys/')
        dec_data = rsakey.decrypt(enc_data)
        if not dec_data:
            self.data_send({'error':True})
            self.sock.close()

        # sing the token with private key
        sig = sign_msg(rsakey, dec_data)
        self.data_send({'dec':dec_data, 'sig':sig, 'error':None})

        ans = self.data_get()
        if ans['error'] != 'True' and ans['ok'] == 'True':
            return True
        else:
            print 'Error:', ans['errmsg'].upper()
            return False


#
#    def auth_up(self, uname, passwd):
#        self.data_send({'type':'pwd', 'user':uname, 'pass':passwd, 'error':None})
#        ans = self.data_get()
#
#        if ans['error'] != 'True' and ans['ok'] == 'True':
#            return True
#        else:
#            return False
#
#

    def interact_vr(self):
        # debugging
        print 'interact vr: '
        ts = time.clock()

        # generic function auth_send() better here?
        if self.opts.auth == 'pwd':
            print 'not supported any more'
            #ret = self.auth_up(self.opts.user, self.opts.pwd)
            ret = False
        else:
            ret = self.auth_pkey(self.opts.keyfile, self.opts.keyid)

        if ret == False:
            print 'auth type="{0}" failed'.format(self.opts.auth)
            self.sock.close()
            sys.exit(0)

        # generate hash
        (shash, anchor) = gen_hash_chain()
        bfactor = gen_bfac()
        server_key = load_key(self.keyfile)
        pubkey = server_key.publickey()
        if pubkey.can_blind():
            bmsg = bytes_to_long(pubkey.blind(anchor, bfactor))
        else:
            print 'algo can not blind!'
            sys.exit(1)

        self.data_send({'bmsg':bmsg,'error':None})
        hdata = self.data_get()

        if hdata['error'] == 'True':
            print 'error returned from server - no signed blinded data!'
            sys.exit(2)

        sbhash = long(hdata['sbhash'])
        usig = long(pubkey.unblind(sbhash, bfactor))
        #debug start
        #print '--------------------------------------------'
        #print 'verifies(bmsg,sbhash): ', pubkey.verify(bmsg, (sbhash,))
        #print '--------------------------------------------'
        #print 'verifies(anchor,usig):  ', pubkey.verify(anchor, (usig,)) 
        #print '--------------------------------------------'
        #debug end

        if  pubkey.verify(anchor, (usig,)) and save_chain(anchor, usig, shash):
            print 'Successfully saved!'
        else:
            print 'Signature verification failure or saving error!'

        self.sock.close()

        te = time.clock()
        print 'timing: ', te-ts



    def interact_vc(self):
        """Do connection interaction with voting server"""
        print 'interact vc:'
        ts = time.clock()

        if self.opts.votingfile is not None and os.path.exists(self.opts.votingfile):
            token = load_chain(self.opts.votingfile)
        else:
            token = load_chain()

        # generate auth dict and send it over
        auth = {'anchor':token['anchor'], 'sig':token['sig'], 'error':False}

        self.data_send(auth)
        # get response
        authok = self.data_get()

        if authok['error'] != 'False':
            print 'error1'
            self.sock.close()
            return

        self.data_send({'dh':'start', 'error':False})
        # receive prime p, generator g and public value pub
        dhvals = self.data_get()

        if dhvals['error'] != 'False':
            self.sock.close()
            return

        dh_val = dh_gen(True, b64decode(dhvals['a.p'].replace('@','=')),
                b64decode(dhvals['a.g'].replace('@','=')))

        # send pub to other end
        self.data_send({'b.pub':b64encode(dh_val.pub).replace('=','@'),'error':False})
        nonce = dh_calc(dh_val, b64decode(dhvals['a.pub'].replace('@','=')))

        # sync send/recv
        self.data_get()

        # wtf?
        vhash = gen_hash_chain(token['shash'], 128)
        # load VC pubkey
        vc_key = load_key(self.keyfile)
        vc_pubkey = vc_key.publickey()

        #debug - to be changed
        vote = '123'

        # gen voting block
        vblock = parse_output_data({'nonce':nonce, 'vote':vote}).rstrip()

        #print 'vblock: ' + vblock
        bmsg = bytes_to_long(vc_pubkey.blind(vblock, bytes_to_long(vhash)))

        self.data_send({'bmsg':bmsg,'error':False})

        tmp = self.data_get()

        if tmp['error'] != 'False':
            print 'error2'
            self.sock.close()
            return

        sbmsg = long(tmp['sbmsg'])
        us_msg = vc_pubkey.unblind(sbmsg, bytes_to_long(vhash))
#        print '--------------------------------------------'
#        print 'bmsg: ', type(bmsg)
#        print '--------------------------------------------'
#        print 'sbmsg: ', type(sbmsg)
#        print '--------------------------------------------'
#        print 'vblock: ', type(vblock)
#        print '--------------------------------------------'
#        print 'verifies(bmsg,sbhash): ', vc_pubkey.verify(long_to_bytes(bmsg),
#                (sbmsg,))
#        print '--------------------------------------------'
#        print 'verifies(anchor,usig):  ', vc_pubkey.verify(bytes_to_long(vblock), (us_msg,)) 
#        print '--------------------------------------------'
        if not check_sig(vc_pubkey, bytes_to_long(vblock), us_msg):
            print 'sig verification failed!'
            self.sock.close()
            return

        # send vhash and vote to server
        self.data_send({'vhash':vhash, 'vblock':bytes_to_long(vblock)})

        # get receipt
        tmp = self.data_get()

        if tmp['error'] == 'True':
            print 'voting failed'
            print 'tmp::',tmp
        else:
            if not check_sig(vc_pubkey, tmp['receipt'], long(tmp['rsig'])):
                print 'receipt signature verification failed!'
            else:
#def save_voting_results(anchor, asig, nonce, vote, receipt, rsig, fname):
                save_voting_results(token['anchor'], token['sig'], vhash, nonce, vote, tmp['receipt'], tmp['rsig'], self.opts.outputfile)
                print 'voting successful'


        self.sock.close()
        te = time.clock()
        print 'timing: ', te-ts



    def check_voting_results(self):
        """Check the correctness of the voting results"""

        if self.opts.tokfile is None or self.opts.vhashf is None or self.opts.votef is None:
            print 'one of the required arguments is not set!'
            return

        if check_voting_results(self.opts.tokfile, self.opts.vhashf, self.opts.votef):
            print 'SUCCESS: voting lists contain your vote!'
            sys.exit(0)
        else:
            print 'FAILURE: your vote will not be counted!'
            sys.exit(1)
