import os
import hashlib
import sqlite3
from gdata.Crypto.PublicKey.RSA import *
from gdata.Crypto.Util.number import bytes_to_long,long_to_bytes
from struct import pack,unpack
import ssl
from M2Crypto import DH

chainlen = 1024

def db_connect(dbtype='sqlite',  dbname='auth', uname='', pwd=''):
    """Database connection subroutine"""
    if dbtype is 'sqlite':
        link = sqlite3.connect('dbs/'+dbname+'.sqlite3')
        link.isolation_level = None
        c = link.cursor()

        # check if table structure exists and create if not 
        c.execute('select count(*) from sqlite_master where type="table" and name=?', (dbname,))
        (data,) = c.fetchone()
        if data < 1:
            if dbname == 'auth':
                db_init_auth(c)
            elif dbname == 'users':
                db_init_users(c)
            elif dbname == 'tokens':
                db_init_tokens(c)
            elif dbname == 'voting':
                db_init_voting(c)
            else:
                # unknown db - no init
                pass


    else:
        # some error msg
        #print 'error: not supported db string used!'
        os.exit(1)

    return c


def db_init_auth(c):
    """Initialize table auth"""
    close=False
    if not c: 
        c = db_connect()
        close=True

    c.execute('create table auth (id INTEGER PRIMARY KEY ASC, pkey blob)')

    if close:
        c.close()


def db_init_users(c=None):
    """Initialize table users"""
    close=False
    if not c:
        c = db_connect('sqlite', 'users')
        close=True

    c.execute('create table users (id INTEGER PRIMARY KEY ASC, username TEXT, first TEXT, last TEXT, city TEXT, passwd TEXT)')

    if close:
        c.close()



def db_init_tokens(c=None):
    """Initialize table tokens"""
    close=False
    if not c:
        c = db_connect('sqlite', 'tokens')
        close=True

    c.execute('create table tokens (uid INTEGER PRIMARY KEY ASC, nonce TEXT, sig TEXT)')

    if close:
        c.close()



def db_init_voting(c=None):
    """Initialize table voting"""
    close=False
    if not c:
        c = db_connect('sqlite', 'voting')
        close=True

    c.execute('create table voting (id INTEGER PRIMARY KEY ASC,'+
            'anchor TEXT, sig TEXT, vhash TEXT, nonce TEXT,'+
            'vote TEXT, receipt TEXT, counter INTEGER)')

    if close:
        c.close()



def gen_bfac(size=32):
	"""Generate random blinding factor of given size"""
	return bytes_to_long(os.urandom(size))



def enc_start(sock, server=False):
    path = os.path.dirname(os.path.abspath(__file__))+'/../keys/'
    if not server: 
        ssl_sock = ssl.wrap_socket(sock, ca_certs=path+'/ca_certs', cert_reqs=ssl.CERT_REQUIRED)
    else :
        ssl_sock = ssl.wrap_socket(sock, server_side=True,
                certfile=path+'/server.pem',
                keyfile=path+'/server.pem',
                ssl_version=ssl.PROTOCOL_TLSv1)

    return ssl_sock



def gen_hash_chain(shash=None, depth=512):
    """Generate a chain of sha512 hashes with supplied depth"""
    # update with 512bit randomness from urandom
    if shash is None:
        sthash = hash_calc();
    else:
        sthash = shash

    # get starting hash
    anchor = chain_calc(sthash, None, False, depth)
    if shash is None:
        return (sthash, anchor)
    else:
        return anchor



def chain_calc(shash, anchor=None, check=False, depth=512):
    """Calculate a new chain or test if given hash results in anchor within depth"""
    if check and anchor is None:
        return False

    for _ in range(depth):
        shash = hash_calc(shash)
        if check and shash == anchor:
            return _

    if check:
        return False
    else:
        return shash


def chain_depth(shash, anchor, count=True, depth=512, tdepth=0):
    """Check if shash calculates to anchor in max depth"""
    if tdepth > depth or depth < 1:
        return False
    else:
        tmphash = hash_calc(shash)
        if count is True and tmphash != anchor:
            return chain_depth(tmphash, anchor, count, depth, tdepth+1)
        elif count is False and depth != tdepth:
            return chain_depth(tmphash, anchor, count, depth, tdepth+1)
        elif count is True and tmphash == anchor:
            return tdepth
        elif count is False and depth == tdepth:
            return tmphash
        else:
            print 'should never happen'
            return False



def hash_calc(lhash=None):
    thash = hashlib.new('sha512')
    if lhash == None:
        thash.update(os.urandom(64))
    else:
        thash.update(lhash)

    shash = thash.hexdigest()
    return shash



def generate_key(keyfile, ktype='priv', savedir='keys', keysize=2048):
    """Generate a (public,private) key pair"""
    if not (os.path.exists(savedir) and os.path.isdir(savedir)):
        os.makedirs(savedir)

    newkey = generate(keysize, os.urandom)
    fwrite(savedir+'/'+keyfile+'.priv', 'wb', str(newkey.__getstate__()), True)

    if ktype == 'priv':
        pub = newkey.publickey()
        fwrite(savedir+'/'+keyfile+'.pub', 'wb', str(pub.__getstate__()), True)

    return newkey



def load_key(keyfile='testkey', ktype='priv', loaddir='keys'):
    """Load a key from file"""
    if keyfile.find('/') != -1:
        fname = keyfile
    else:
        fname = loaddir+'/'+keyfile+'.'+ktype

    if os.path.exists(fname) and os.path.getsize(fname):
        fd = open(fname, 'rb')
        tmpkey = fd.read()
        fd.close()
        return eval_key(tmpkey)
    elif ktype == 'pub':
        return None
    else:
        return generate_key(keyfile, ktype)


def load_key_db(uid):
    c = db_connect()
    c.execute('select pkey from auth where id=?', (uid,))

    res = c.fetchall()
    c.close()

    if len(res) != 1:
        return None

    (pkey,) = res[0]
    if pkey:
        pubkey = eval_key(pkey)
        return pubkey
    else:
        return None



def eval_key(pkey):
    rsakey = RSAobj()
    # possibly replace eval? (is ugly/dangerous this way)
    rsakey.__setstate__(eval(pkey))

    return rsakey



def check_sig(key, msg, sig):
	"""Check the signature of a given message"""
	if not type(sig) == long:
		sig = long(sig)

	return key.verify(msg, (sig,))



def in_hash_chain(vhash, anchor):
	"""Check if vhash is a valid part of the (shash,anchor) chain"""
	return chain_calc(vhash, anchor, True)



def save_vote(vote):
	"""Save voting data and return a receipt"""
	return 'receipt'



def sign_msg(key, msg):
	"""Sign a message and return signature"""
	smsg = key.sign(msg, '')
	return str(smsg[0])



def parse_input_data(data):
	"""unpack transmitted data"""
	D = {}
	for x in data.split('|'):
		(k,v) = x.split('=')
		D[k] = v	

	return D



def parse_output_data(data):
	"""
	Pack data after transmission
	example:
	data = {'server':'mpilgrim', 'database':'master', 'uid':'sa', 'pwd':'secret'}
	"""
	return '|'.join(["%s=%s" % (k, v) for k, v in data.items()])



def dh_gen(toset=False, p=96, g=13):
	if toset: 
		dh_val = DH.set_params(p,g)
	else:
		dh_val = DH.gen_params(p,g)

    	dh_val.gen_key()
	return dh_val



def dh_calc(dh_val, pubk):
	return bytes_to_long(dh_val.compute_key(pubk))


def save_chain(anchor, sig, shash, vblock='vblock.txt'):
    """Save voting token into a local file"""
    data = parse_output_data({'anchor':anchor, 'sig':sig, 'shash':shash})

    return fwrite(vblock, 'wb', data)



def fwrite(fname, mode, data, force=False):
    """writing data to file with check if file exists"""

    if os.path.exists(fname) and not force:
        print 'filename already exists.'
        ans = raw_input('Overwrite it? Yes/No: ').lower()
        if ans == 'n' or ans == 'no':
            return False


    try: 
        fp = open(fname, mode)
        fp.write(data)
        fp.close()
        return True
    except Exception, e:
        import traceback
        print traceback.format_exc()
        return False



